# FedexDemo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.6.

## Assignment question & answer

*Email should be validated but there are various ways of accomplishing this. So, show us what you consider as a proper email validation.*

I choose the following solution, by using regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

This solution work in 99.99% of all email, also for 'newer' domains for example .amsterdam etc. It checks also if there is a TLD. 

You could use the official email validator of Angular. But this validator also thinks test@test (NO TLD!) is a valid mail address. 

NOTE: E2E testing through protractor not covered (no subject of assignment)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

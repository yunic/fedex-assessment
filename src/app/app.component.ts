import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailValidator, passwordValidator } from '../services/validators';
import { DemoApiService } from '../services/api/demoapi.users';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private apiService: DemoApiService) {}

  signupForm = new FormGroup({
    firstName: new FormControl('', [
      Validators.required
    ]),
    lastName: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.required,
      emailValidator()
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      passwordValidator('firstName','lastName')
    ])
  });
  
  onSubmit() {
    this.signupForm.controls["password"].updateValueAndValidity();
    if(this.signupForm.valid) {
      const {firstName,lastName,email} = this.signupForm.value; 
      this.apiService.postUser({firstName,lastName,email}).subscribe((res)=>{
        console.info('post to users successful:', res);
      });
    }
  }
}

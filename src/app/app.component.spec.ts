import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DemoApiService } from 'src/services/api/demoapi.users';
import { of } from 'rxjs';

describe('Demo app component, signup from', () => {
  let component, mockApiService, fixture; 

  beforeEach(async () => {
    mockApiService = jasmine.createSpyObj(['postUser']);
    mockApiService.postUser.and.returnValue(of({data:'dummy'}));

    await TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatFormFieldModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        {provide: DemoApiService, useValue: mockApiService }
      ]
    }).compileComponents();

    
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should create the app / demo signup form', () => {
    expect(component).toBeTruthy();
  });

  it('should NOT submit empty form', () => {
    component.onSubmit();
    expect(mockApiService.postUser).not.toHaveBeenCalled();
  });

  it('should NOT submit faulty form', () => {
    component.signupForm.setValue({
      firstName: "Test",
      lastName: "Test",
      email: "test@test",
      password: "123"
    });
    component.onSubmit();
    expect(mockApiService.postUser).not.toHaveBeenCalled();
  });

  it('should submit valid form', () => {
    component.signupForm.setValue({
      firstName: "Test",
      lastName: "Test",
      email: "test@test.nl",
      password: "12345678"
    });
    component.onSubmit();
    expect(mockApiService.postUser).toHaveBeenCalled();
  });
});

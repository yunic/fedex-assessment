import { DemoApiService } from './demoapi.users';
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('demoApi', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [DemoApiService],
            imports: [
                HttpClientTestingModule
            ],
        });
    });

    it('expects service to post user',
        inject([HttpTestingController, DemoApiService],
            (httpMock: HttpTestingController, service: DemoApiService) => {
                const demoUser = { firstName: 'test', lastName: 'test2', email: 'test@test.nl' };
                const dummyReturn = { data: null };

                service.postUser(demoUser).subscribe(data => {
                    expect(data).toEqual(dummyReturn);
                });
         
                const req = httpMock.expectOne('https://demo-api.now.sh/users');
                expect(req.request.method).toEqual('POST');
                req.flush(dummyReturn);
            })
    );
});

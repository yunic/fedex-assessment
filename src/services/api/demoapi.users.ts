import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUser } from '../../interfaces/user';

@Injectable({
    providedIn: 'root'
})
export class DemoApiService {
    apiURL: string = 'https://demo-api.now.sh/users';

    constructor(private http: HttpClient) { }

    /**
     * Post user to API
     * @param user 
     */
    postUser(user: IUser) {
        return this.http.post(this.apiURL, user);
    }
}
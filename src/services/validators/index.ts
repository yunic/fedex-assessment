import { emailValidator } from './email/email.validator';
import { passwordValidator } from './password/password.validator';

export {
    emailValidator,
    passwordValidator
}
import { ValidatorFn, AbstractControl } from '@angular/forms';

//REGEX from https://emailregex.com/ works 99% of all cases. 
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function emailValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const validEmail = !emailRegex.test(control.value);
        return validEmail ? { validEmail: { value: control.value } } : null;
    };
}
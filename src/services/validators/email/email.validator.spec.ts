import { emailValidator } from './email.validator';
import { FormControl } from '@angular/forms';

describe('emailValidator', () => {
  const validEmail = 'test@test.com';
  const inValidEmail = 'test@test.com.';
  const validHTML5mail = 'test@test'
  const validator = emailValidator();

  it(`should validate ${validEmail}`, () => {
    expect(validator(new FormControl(validEmail))).toEqual(null)
  });

  it(`should NOT validate ${inValidEmail}`, () => {
    expect(validator(new FormControl(inValidEmail))).toEqual({ validEmail: { value: inValidEmail } })
  });

  it(`should NOT validate HTML5 valid email (NO TLD) ${validHTML5mail}`, () => {
    expect(validator(new FormControl(validHTML5mail))).toEqual({ validEmail: { value: validHTML5mail } })
  });
});

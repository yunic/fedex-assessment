import { ValidatorFn, AbstractControl } from '@angular/forms';

export function passwordValidator(fieldNameFirstname: string, fieldNameLastname: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const formGroup = control.parent;

        if(formGroup === undefined) return null; 

        const noFirstNameLastName = control.value.search(formGroup.value[fieldNameFirstname]) === -1 && 
        control.value.search(formGroup.value[fieldNameLastname]) === -1;

        return !noFirstNameLastName ? { noFirstNameLastName: { value: control.value } } : null;
    };
} 
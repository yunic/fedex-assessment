import { passwordValidator } from './password.validator';
import { FormControl, FormGroup } from '@angular/forms';

describe('passwordValidator', () => {
 
  const validator = passwordValidator('firstname','lastname');
  
  it(`should validate password with different firstname lastname`, () => {
    const testForm = new FormGroup({
      firstname: new FormControl('test'),
      lastname: new FormControl('test'),
      password: new FormControl('123', [
        passwordValidator('firstname','lastname')
      ])
    });
    expect(validator(testForm.controls.password)).toEqual(null)
  });

  it(`should NOT validate password with firstname lastname`, () => {
    const testForm2 = new FormGroup({
      firstname2: new FormControl('testa'),
      lastname2: new FormControl('testb'),
      password2: new FormControl('testatestc', [
        passwordValidator('firstname2','lastname2')
      ])
    });
    expect(validator(testForm2.controls.password2)).toEqual({ noFirstNameLastName: { value: 'testatestc' } })
  });
});
